Selenium Project

Lenguaje: Java
Frameworks: Selenium, TestNg
Dependencias: Maven

Instrucciones

	Deben tener instalado en la compu el java JDK para poder compilar y correr el codigo. Maven se encarga de bajar las dependencias del proyecto al compilar el codigo. Deben instalar el plugin de Maven 
    en el ide que vayan a utilizar. En eclipse ya viene instalado.
	
	Certificarse de tener el Chromedriver instalado en la carpeta base del proyecto. Ya esta subido a bitbucket, asi que no deberian tener que bajarlo ni moverlo.
	
Tests

  1- Suscribirse al newsletter:
  	Pasos 
  		- Ingresa un nombre
  		- Selecciona el tipo Empresa
  		- Ingresa un email
  		- Hace click en Suscribir
  		- Chequea si aparece el mensaje "Gracias por Suscribirse"
  	
  2- Login con usuario y contraseņa invalidos:
  	Pasos
  		- Hace click en ingresar en la home page
  		- En la pagina de login ingresa un email invalido
  		- Ingresa una contraseņa invalida
  		- Chequea que aparezca el mensaje de error "La contraseņa ingresada es incorrecta."


