package com.Experta.Tests.Login;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Experta.Pages.Home.Home;
import com.Experta.Pages.Login.Login;
import com.Experta.Tests.Base.TestBase;
import com.Experta.Util.WaitTool;

public class tests_Login extends TestBase {
	
	private WebDriver driver;

	@BeforeMethod
	public void setUp() {
		driver = getDriver();
	}

	/**
	 * Entra a la pagina de login e ingresa un usuario y contraseña invalidos
	 * Chequea que aparezca el mensaje de contraseña incorrecta.
	 * 
	 */
	@Test
	public void test_invalid_login() {
		Home home = PageFactory.initElements(driver, Home.class);
		home.open();
		WaitTool.waitForJQueryProcessing(driver, 3);

		Login login = home.clickIngresarLogin();
		WaitTool.waitForJQueryProcessing(driver, 3);

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}

		// Los strings de user y pass idealmente estarian en un helper y no hardcodeados aca.
		login.insertUser("test@gmail.com");
		login.insertPass("123456");
		login.clickIngresar();
		WaitTool.waitForJQueryProcessing(driver, 3);

		assertEquals(login.getErrorMessage(), "La contraseña ingresada es incorrecta.");
	}

	@AfterMethod
	public void tearUp() {
		driver.quit();
	}

}
