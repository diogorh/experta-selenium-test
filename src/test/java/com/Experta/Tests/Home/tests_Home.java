package com.Experta.Tests.Home;

import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Experta.Pages.Home.Home;
import com.Experta.Tests.Base.TestBase;
import com.Experta.Util.WaitTool;

public class tests_Home extends TestBase {

	private WebDriver driver;
	
	@BeforeMethod
	public void setUp() {
		driver = getDriver();
	}
	
	/**
	 * Entra a la homepage y se inscribe al newsletter
	 * Chequea que aparezca el mensaje gracias por suscribirse.
	 * 
	 */
	@Test
	public void test_subscribe_newsletter() {
		Home home = PageFactory.initElements(driver, Home.class);
		home.open();
		WaitTool.waitForJQueryProcessing(driver, 3);
		
		home.ingresarNombre("Pepe");
		home.selectTipo("Empresa");
		home.ingresarEmail("pepe@gmail.com");
		home.clickSuscribirme();
		
		assertTrue(home.isSusbscribedMsgVisible());
	}
	
	@AfterMethod
	public void tearUp() {
		driver.quit();
	}
	
	
}
