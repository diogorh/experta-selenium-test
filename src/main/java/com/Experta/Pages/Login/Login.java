package com.Experta.Pages.Login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.Experta.Pages.Base.PageBase;

public class Login extends PageBase {
	
	
	@FindBy(id = "usuario")
	WebElement usuario;
	
	@FindBy(id = "password")
	WebElement contrasena;
	
	@FindBy(name = "submit")
	WebElement ingresar;
	
	@FindBy(xpath = "//div[@class='error']/ul/li")
	WebElement errorMsg;
	
	public Login(WebDriver driver) {
		super(driver);
		URL = "https://www.experta.com.ar/ARTServicio/ART/Transaccion/LoginInput.lnk";
	}
	
	
	public void insertUser(String user) {
		usuario.click();
		usuario.sendKeys(user);
	}
	
	public void insertPass(String password) {
		contrasena.sendKeys(password);
	}
	
	public void clickIngresar() {
		ingresar.click();
	}
	
	public String getErrorMessage() {
		return errorMsg.getText();
		
	}

}
