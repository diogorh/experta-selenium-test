package com.Experta.Pages.Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PageBase {

	private WebDriver driver;
	public String URL;
	
	public PageBase(WebDriver wdriver) {
		driver = wdriver;
	}
	
	//Opens the default page
	public void open() {
		driver.get(URL);
	}
	
	//Returns the page URL
	public String getURL() {
		return driver.getCurrentUrl();
	}
	
	//Is text present in WebElement
	public boolean isTextPresent(By by, String text) {
		return driver.findElement(by).getText().equals(text);
	}
	
	//Returns true if element is visible
	public boolean isElementVisible(By by) {
		return driver.findElement(by).isDisplayed();
	}
	
}
