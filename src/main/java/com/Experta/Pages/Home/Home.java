package com.Experta.Pages.Home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.Experta.Pages.Base.PageBase;
import com.Experta.Pages.Login.Login;
import com.Experta.Util.WaitTool;

public class Home extends PageBase {
	
	private WebDriver driver;
	
	@FindBy(className = "headerimg")
	private WebElement ingresarLogin;
	
	@FindBy(xpath = "//div[@class='btngo'][1]")
	private WebElement ingresarCapacitacion;
	
	@FindBy(id = "nombre")
	private WebElement nombreNewsLetter;
	
	@FindBy(id = "tipo")
	private WebElement typeNewsLetter;
	
	@FindBy(id = "email")
	private WebElement emailNewsLetter;
	
	@FindBy(className = "btnsubscribe")
	private WebElement suscribirmeNewsLetter;
	
	public Home(WebDriver wDriver) {
		super(wDriver);
		URL = "https://www.experta.com.ar/";
		driver = wDriver;
	}
	
	public Login clickIngresarLogin() {
		ingresarLogin.click();
		return PageFactory.initElements(driver, Login.class);
	}
	
	public void clickIngresarCapacitacion() {
		ingresarCapacitacion.click();
	}
	
	public void ingresarNombre(String name) {
		nombreNewsLetter.sendKeys(name);
	}
	
	public void selectTipo(String tipo) {
		Select select = new Select(typeNewsLetter);
		select.selectByValue(tipo);
	}
	
	public void ingresarEmail(String email) {
		emailNewsLetter.sendKeys(email);
	}
	
	public void clickSuscribirme() {
		suscribirmeNewsLetter.click();
	}
	
	public boolean isSusbscribedMsgVisible() {
		By message = By.id("ty");
		WaitTool.waitForElement(driver, message, 6);

		return isElementVisible(message);
	}
}
