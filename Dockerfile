FROM openjdk:8-jre-slim


# Agrego el .jar del proyecto y las dependencias
ADD  target/Legales.jar /usr/share/tag/legales.jar
ADD  target/libs /usr/share/tag/libs

#ADD  target/libs/testng-6.14.3.jar /usr/share/testng-6.14.3.jar
COPY target/libs/testng-6.14.3.jar /var/jenkins_home/testng

# Agrego los test suites
ADD ./smoke_test.xml /usr/share/tag/smoke_test.xml

ENV CLASSPATH /var/jenkins_home/testng/testng-6.14.3.jar
ENV TESTNG_HOME /var/jenkins_home/testng

# Command line to execute the test
ENTRYPOINT /usr/bin/java -cp /usr/share/tag/Legales.jar:/usr/share/tag/libs/* -DseleniumHubHost=$SELENIUM_HUB org.testng.TestNG /usr/share/tag/smoke_test.xml